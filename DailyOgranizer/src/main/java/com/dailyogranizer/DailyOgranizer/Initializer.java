package com.dailyogranizer.DailyOgranizer;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import model.notifications.NotificationTimeTask;

public class Initializer implements ServletContextListener {
	private static final int TIMER_TIME = 2000*60;

	@Override
	public void contextDestroyed(ServletContextEvent argm) {
		// TODO Auto-generated method stub

	}

	@Override
	public void contextInitialized(ServletContextEvent argm) {
		
		TimerTask timerTask = new NotificationTimeTask();
		Timer timer = new Timer(true);
		timer.scheduleAtFixedRate(timerTask, 0, TIMER_TIME);
		System.out.println("TimerTask begins! :" + new Date());

	}

}
