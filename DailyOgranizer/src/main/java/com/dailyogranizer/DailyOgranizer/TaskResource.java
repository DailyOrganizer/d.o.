package com.dailyogranizer.DailyOgranizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

import service.CategoryService;
import service.DataBaseStatus;
import service.TaskService;
import model.Category;
import model.Task;

@Path("/task")
public class TaskResource {

	CategoryService categoryService = new CategoryService();
	TaskService taskService = new TaskService();

	@Path("/{taskId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response getTasks(@PathParam("taskId") int taskId) {

		Task task = taskService.getTask(taskId);

		if (task != null) {
			return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*").entity(task).build();
		} else {
			return Response.status(Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request", 403)).build();

		}
	}

	@Path("/{categoryId}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTask(@PathParam("categoryId") int categoryId, Task task) {
		Task newTask = taskService.addAndReturnTask(categoryId, task);
		if (newTask != null)
			return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*").entity(newTask).build();
		else {
			return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("You are sending invalid data", 403)).build();
		}

	}

	@Path("/{taskId}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTask(@PathParam("taskId") int taskId, Task task) {
		Task updatedTask = taskService.updateTask(taskId, task);
		if (updatedTask != null) {
			return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*").entity(updatedTask).build();
		} else {
			return Response.status(Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("This update failed", 403)).build();
		}
	}

	@Path("/{taskId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteTask(@PathParam("taskId") int taskId) {
		boolean isDeleted = taskService.deleteTask(taskId);
		if (isDeleted) {
			return Response.status(Status.OK).header("Access-Control-Allow-Origin", "*").build();

		} else {
			return Response.status(Status.UNAUTHORIZED).header("Access-Control-Allow-Origin", "*").build();
		}
	}

}
