package com.dailyogranizer.DailyOgranizer;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

import model.Category;
import model.SearchTable;
import model.Task;
import service.SearchTableService;
import service.TaskService;

@Path("/search")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SortByDateResource {
	private SearchTableService searchTableService = new SearchTableService();
	
	@Path("/{type}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTasks(@PathParam("type") String type,Date date) {

		ArrayList<Task> tasks= searchTableService.search(type,date);
		SearchTable searchTable = new SearchTable();
		searchTable.setTasks(tasks);
		if (tasks != null) {
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(searchTable).build();
		} else {
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("No results found", 403))
					.build();

		}
	}
	
	@Path("/{userId}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response getTasks(@PathParam("userId") int userId) {

		ArrayList<Task> tasks= searchTableService.searchAll( userId);
		System.out.println(tasks);
		SearchTable searchTable = new SearchTable();
		searchTable.setTasks(tasks);

		for(Task task : tasks)
		{
			System.out.println(task);
		}
		if (tasks != null) {
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(searchTable).build();
		} else {
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("No results found", 403))
					.build();

		}
	}

}
