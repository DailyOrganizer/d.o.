package com.dailyogranizer.DailyOgranizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;
import com.dailyogranizer.DailyOgranizer.exceptions.MalformedJsonException;

import model.User;
import service.UserService;

@Path("/register")
public class RegisterResource {

	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response registerUser(User user) {
		
		System.out.println("Krasi");
		UserService userService = new UserService();
		
		if(user.getUserName() == null 
				|| user.getEmail() == null 
				|| user.getPassword() == null)
		{
			return Response.status(Status.CONFLICT).
					header("Access-Control-Allow-Origin :", "true")
					.entity(new ErrorMessage("The JSon is malformed",403))
					.build();
			}
						
		if (userService.isUserValid(user)) {

			User registeredUser = userService.addUserAndReturnIt(user);
			userService.addDefaultCategories(registeredUser);
			return Response.status(Status.CREATED).
					header("Access-Control-Allow-Origin :", "true")
					.entity(registeredUser)
					.build();

		} else
		{
			return Response.status(Status.CONFLICT).
					header("Access-Control-Allow-Origin :", "true")
					.entity(new ErrorMessage("The username is already taken",409))
					.build();
		}
			
	}

}
