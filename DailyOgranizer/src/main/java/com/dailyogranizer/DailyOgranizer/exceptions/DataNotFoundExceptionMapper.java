package com.dailyogranizer.DailyOgranizer.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class DataNotFoundExceptionMapper implements ExceptionMapper<DataNotFoundException>{

	@Override
	public Response toResponse(DataNotFoundException exception) {
		ErrorMessage error = new ErrorMessage(exception.getMessage(),404);
		return Response.status(Status.NOT_FOUND)
				.header("Access-Control-Allow-Origin", "*")
				.entity(error)
				.build();
	}

}
