package com.dailyogranizer.DailyOgranizer.exceptions;

public class DataNotFoundException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5776619169870941350L;
	
	public DataNotFoundException(String message)
	{
		super(message);
	}

}
