package service;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.joda.time.DateTime;

import com.dailyogranizer.DailyOgranizer.exceptions.DataNotFoundException;

import database.HibernateUtil;
import model.Task;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

public class SearchTableService {
	private final SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
	private final static String TYPE_DAY = "day";
	private final static String TYPE_WEEK = "week";
	private final static String TYPE_MONTH = "month";
	private final static int CONST_DAY = 24;
	private final static int CONST_WEEK = 7;
	private final static int CONST_MONTH = 30;


	private Date convertDate(String type, Date date) {
		DateTime tempDate = new DateTime(date);
		switch (type) {
		case TYPE_DAY:
			System.out.println("DAY");
			tempDate = tempDate.plusHours(CONST_DAY);
			System.out.println(tempDate);
			break;
			
		case TYPE_WEEK:
			tempDate = tempDate.plusDays(CONST_WEEK);
			System.out.println(tempDate);
			break;
			
		case TYPE_MONTH:
			tempDate = tempDate.plusDays(CONST_MONTH);
			System.out.println(tempDate);
			break;

		}
		Date newDate = tempDate.toDate();
		return newDate;

	}

	public synchronized ArrayList<Task> search(String type, Date date) {

		Session session = sessionFactory.openSession();
		System.out.println(date);
		Date newDate = convertDate(type, date);
		System.out.println(newDate);
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Task where dueDate >=  :dueDate AND dueDate < :nextDay ");
			query.setParameter("dueDate", date);
			query.setParameter("nextDay", newDate);
			ArrayList<Task> tasks = (ArrayList<Task>) query.list();
			System.out.println(tasks);
			if (tasks != null) {
				session.getTransaction().commit();
				return tasks;
			}
			session.getTransaction().commit();
			return null;
		} catch (Exception e) {
			try {
				transaction.rollback();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} finally {
			session.close();
		}
		return null;
	}

	public ArrayList<Task> searchAll(int userId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		ArrayList<Task> al = new ArrayList<Task>();
		System.out.println("ASD");
		try {
			
			transaction = session.beginTransaction();
			Query query = session.createQuery("from Task");
			ArrayList<Task> tasks = (ArrayList<Task>) query.list();
			System.out.println(tasks  + "   sds");
			if (tasks != null) {	
			for(Task task : tasks)
			{
				if(task.category().user().getUserId() == userId)
				{
					al.add(task);
				}
			}

			session.getTransaction().commit();
			return al;

			}
			else{
			session.getTransaction().commit();
			return al;
			}
		} catch (Exception e) {
			try {
				transaction.rollback();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} finally {
			session.close();
		}
		return al;
	}

}
