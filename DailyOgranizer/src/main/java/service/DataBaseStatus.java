package service;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class DataBaseStatus {

	private boolean isCommited;
	private String message;
	
	public DataBaseStatus() {
		
	}
	
	public DataBaseStatus(boolean isCommited,String message) {
		
		this.isCommited = isCommited;
		this.message = message;
	}

	public boolean isCommited() {
		return isCommited;
	}

	public void setCommited(boolean isCommited) {
		this.isCommited = isCommited;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	@Override
	public String toString() {
		
		return isCommited+message;
	}
}
