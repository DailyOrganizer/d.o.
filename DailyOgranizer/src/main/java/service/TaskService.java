package service;

import java.util.ArrayList;
import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dailyogranizer.DailyOgranizer.exceptions.DataNotFoundException;

import model.Task;
import model.Category;
import model.SubTask;
import database.HibernateUtil;

public class TaskService {

	private final SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
	
	public Task getTask(int id) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();			
			Task task = (Task) session.get(Task.class, id);
			if (task != null) {
				session.getTransaction().commit();
				return task;
			}
			session.getTransaction().commit();
			throw new DataNotFoundException("There is no category with such id");
			
		}catch (Exception e) {
			try
			{
				transaction.rollback();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
	      }finally 
			{
	         session.close(); 
	      }
		return null;
	}

	public  synchronized Task  addAndReturnTask(int categoryId, Task task) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Category category = (Category) session.get(Category.class, categoryId);
			if (category != null) {
				task.setCategory(category);
				category.tasks().add(task);
				session.save(task);
				session.getTransaction().commit();
				return task;

			} else {
				session.getTransaction().commit();
				return null;
			}
			}catch (Exception e) {
				try
				{
					transaction.rollback();
				}catch(Exception ex)
				{
					ex.printStackTrace();
				} 
		      }finally 
				{
		         session.close(); 
		      }
			return null;

	}
	
	public synchronized boolean deleteTask(int taskId)
	{
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Task task = (Task) session.get(Task.class, taskId);
			if(task != null)
			{
				session.delete(task);
				session.getTransaction().commit();
				return true;
			}
			else
			{
				session.getTransaction().commit();
				return false;
			}
			}catch (Exception e) {
				try
				{
					transaction.rollback();
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
		      }finally 
				{
		         session.close(); 
		      }
			return false;
	}
	
	public synchronized ArrayList<SubTask> getsubTasks(int taskId)
	{
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Query query = session.createQuery("new ArrayList<SubTask> from SubTask where subTaskId = :taskId");
			query.setCacheable(true);
			query.setParameter("taskId", taskId);
			@SuppressWarnings("unchecked")
			ArrayList<SubTask> arrayList = (ArrayList<SubTask>) query.list();
			session.getTransaction().commit();
			return arrayList;
		}catch (HibernateException e) {
			try
			{
				transaction.rollback();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
	      }finally 
			{
	         session.close(); 
	      }
		return null;
	}
	
	
	 public  synchronized Task updateTask(int taskId,Task task) {
			
			Session session = sessionFactory.openSession();
			Transaction transaction = null;
			try
			{
				transaction = session.beginTransaction();
				Task taskFromDb = (Task) session.get(Task.class, taskId);
				if (taskFromDb != null) 
				{	
					taskFromDb.setAllField(task.getTaskName(), 
							task.getTaskDescription(), 
							task.getDueDate(), 
							task.getNotificationDate(), 
							task.isTaskChecked()
							);
					session.update(taskFromDb);
					session.getTransaction().commit();
					return taskFromDb;
				}
			}catch (Exception e) {
			try {
				transaction.rollback();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
				} finally {
					session.close();
				}
			return null;
			

	 }

	
	
	
	

}
