package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.SelectBeforeUpdate;

@XmlRootElement
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "Tasks")
@SelectBeforeUpdate
public class Task implements Serializable {

	private static final long serialVersionUID = -2990417085467101251L;

	@OneToMany(mappedBy = "task", fetch = FetchType.EAGER)
	@Cascade({CascadeType.DELETE})
	private List<SubTask> subTasks = new ArrayList<SubTask>();

	@ManyToOne
	@JoinColumn(name = "categoryid")
	@NotFound(action = NotFoundAction.IGNORE)
	private Category category;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int taskId;
	
	private String taskName;
	private String taskDescription;
	private Date notificationDate;
	private Date dueDate;
	private boolean isTaskChecked;
	private boolean isNotificated;

	public boolean isNotificated() {
		return isNotificated;
	}
	public void setNotificated(boolean isNotificated) {
		this.isNotificated = isNotificated;
	}
	public List<SubTask> getSubTasks() {
		return subTasks;
	}
	public List<SubTask> subTasks()
	{
		return subTasks;
	}

	public void setSubTasks(List<SubTask> subTasks) {
		this.subTasks = subTasks;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	@Column(name = "taskid")
	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	@Column(name = "taskname")
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	
	public Category category()
	{
		return category;
	}

	@Column(name = "taskdescription")
	public String getTaskDescription() {
		return taskDescription;
	}

	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	@Column(name = "notificationdate")
	public Date getNotificationDate() {
		return notificationDate;
	}

	public void setNotificationDate(Date notificationDate) {
		this.notificationDate = notificationDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}


	@Column(name = "duedate")
	public Date getDueDate() {
		return dueDate;
	}

	
	@Column(name = "istaskchecked")
	public boolean isTaskChecked() {
		return isTaskChecked;
	}
	
	public void setTaskChecked(boolean isTaskChecked) {
		this.isTaskChecked = isTaskChecked;
	}
	public void setAllField(String taskName,String taskDescripton,Date dueDate,Date notificationDate,boolean isTaskChecked)
	{
		this.taskName = taskName;
		this.taskDescription = taskDescripton;
		this.dueDate = dueDate;
		this.notificationDate = notificationDate;
		this.isTaskChecked = isTaskChecked;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + taskId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Task other = (Task) obj;
		if (taskId != other.taskId)
			return false;
		return true;
	}

	
}
