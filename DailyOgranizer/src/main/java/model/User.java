package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.SelectBeforeUpdate;



@XmlRootElement
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "Users" )
@SelectBeforeUpdate
@NamedQuery(name = "Users_byName", query = "from User where userName = ?")
public class User implements Serializable {

	private static final long serialVersionUID = -20393249040442336L;
	
	private String firstName;
	

	private String lastName;
	
	@NotNull
	@Column(unique = true, nullable = false)
	private String userName;
	
	@NotNull
	@Column(nullable = false)
	private String password;
	
	@NotNull
	@Column(unique = true, nullable = false)
	private String email;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userId;

	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER)
	@Cascade({CascadeType.DELETE})
	private List<Category> categories = new ArrayList<Category>();
	
	public User() {}
	
	
	public User(String userName,String password,String email) {
		super();
		this.userName = userName;
		this.password = password;
		this.email = email;
	}




	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}


	@Column(name = "firstname")
	public String getFirstName() {
		return firstName;
	}


	public List<Category> getCategories() {
		return categories;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Column(name = "lastname")
	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Column(name = "username")
	public String getUserName() {
		return userName;
	}
	

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "password")
	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "userid")
	public int getUserId() {
		return userId;
	}


	public void setUserId(int userId) {
		this.userId = userId;
	}



	public void addCategory(Category category) {
		// TODO Auto-generated method stub
		
	}
	
	public void setAllFields(String userName,String password,String email,
			String firstName,String lastName)
	{
		this.userName = userName;
		this.password = password;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return categories.toString()+""+userName;
	}


	public Object getListOfCategories() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + userId;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	
}