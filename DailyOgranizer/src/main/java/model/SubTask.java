package model;

import java.io.Serializable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.SelectBeforeUpdate;

@XmlRootElement
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "Subtasks")
@SelectBeforeUpdate

public class SubTask implements Serializable {

	private static final long serialVersionUID = -4102579960587614099L;

	public SubTask() {
	}
	private String subTaskName;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int subTaskId;
	private boolean subTaskChecked;

	@ManyToOne
	@JoinColumn(name = "taskid")
	@NotFound(action = NotFoundAction.IGNORE)
	private Task task;

	@Column(name = "subtaskid")
	public int getSubTaskId() {
		return subTaskId;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	@Column(name = "subtaskname", nullable = false)
	public String getSubTaskName() {
		return subTaskName;
	}

	public void setSubTaskName(String subTaskName) {
		this.subTaskName = subTaskName;
	}

	public void setSubTaskId(int subTaskId) {
		this.subTaskId = subTaskId;
	}

	@Column(name = "issubtaskchecked")
	public boolean isSubTaskChecked() {
		return subTaskChecked;
	}
	
	public Task task()
	{
		return task;
	}
	
	public void setSubTaskChecked(boolean subTaskChecked) {
		this.subTaskChecked = subTaskChecked;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + subTaskId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubTask other = (SubTask) obj;
		if (subTaskId != other.subTaskId)
			return false;
		return true;
	}

	
}
