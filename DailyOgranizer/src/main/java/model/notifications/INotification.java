package model.notifications;

public interface INotification {

	void sendEmail();
}
