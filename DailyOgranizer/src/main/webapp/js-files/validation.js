function validate() {
	var username = document.forms["registration-form"]["sign-up-username"].value;
	if (username == null || username.length == 0) {
		alert("Enter correctly username.");
		return false;
	}
	var name = document.forms["registration-form"]["sign-up-name"].value;
	if (name == null || name.length == 0) {
		alert("Enter correctly name.");
		return false;
	}
	var lastName = document.forms["registration-form"]["signup-lastname"].value;
	if (lastName == null || lastName.length == 0) {
		alert("Enter correctly last name.");
		return false;
	}
	var email = document.forms["registration-form"]["sign-up-email"].value;
	if (email == null || email.length == 0) {
		alert("enter email");
		return false;
	} else {
		var boolCheck = false;
		for (var i = 0; i < email.length; i++) {
			if ((email.charCodeAt(i) == 64) && (i != 0)
					&& (i != email.length - 1)) {
				for (var j = i; j < email.length; j++) {
					if ((email.charCodeAt(j) == 46) && (j != email.length - 1)) {
						boolCheck = true;
						break;
					}
				}
			}
			if (boolCheck) {
				break;
			}
		}
		if (boolCheck == false) {
			alert("enter correct email")
			return false;
		}
	}
	
	
	return true;
}