function url(uri) {
	var path = "http://localhost:8080/DailyOgranizer/webapi";
	return path + uri;
}
function getUserID(){
	var userID = sessionStorage.getItem('userId');
	return userID;
}
//server format kum moq format
function dateIn(fromServer){
    var date = "";
    var year = fromServer.slice(0,4);
    var month = fromServer.slice(5,7);
    var day = fromServer.slice(8,10);
    date = day + "/" + month + "/" + year;
    return date;
}
function timeIn(fromServer){
    var clock = '';
    clock = fromServer.slice(11,16);
    return clock;
}
//console.log(dateIn("2014-06-07T13:23:03"));
//console.log(timeIn("2014-06-07T13:23:03"));
//moq format kum server format
function dateOut(dateFromMe, timeFromMe){
    var date = '';
    var day = dateFromMe.slice(3,5);
    var month = dateFromMe.slice(0,2);
    var year = dateFromMe.slice(6,10);
    date = year + "-" + month + "-" + day + "T" + timeFromMe + ":00";
    return date;
}
// console.log(dateOut("06/20/2015", "17:01"));
// date for notification
function dateNotification(dateFromMe, timeFromMe){
    var notifyTime = parseInt(timeFromMe.slice(0,2));
    var notifyMin = timeFromMe.slice(2,5);
    notifyTime -= 1;
    if(notifyTime < 10 && notifyTime >= 0){
    	notifyTime = "0" + notifyTime;
    }
    notifyTime = notifyTime + notifyMin;
    return dateOut(dateFromMe, notifyTime);
}
//console.log(dateNotification("06/20/2015", "17:01"));

$(document).ready(function(){
	var idCategory = null;
	//get all data of user
	function getCategoryData(){
		$('.categoryLists').remove();
		$('.badge').remove();
		$.get(url("/users/") + getUserID(), function(data){
			//	console.log(data);
				for(var j = 0; j < data.categories.length - 1; j+=1){
					if(data.categories[j].categoryName === data.categories[j+1].categoryName){
						continue;
					}
					var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
					'</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
					var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
					$('#categories').append(category);
					$('.sel1').append(sameCategory);
				}
				var greeting = "Hello, " + data.firstName + " " + data.lastName + "!";
				$('#hello').append(greeting);
				
				var currUserName = data.userName;
				$('#currentName').append(currUserName);
				var currPass = data.password;
				$('#currentPass').append(currPass);
				var currEmail = data.email;
				$('#currentEmail').append(currEmail);
			});
	}
	getCategoryData();
	$(document).on('click','.categoryLists', function(){
		idCategory = $(this).attr('id');
		$("#buttonsForCategories").show();
	    $('#inputCategory').hide();
	    $("#nameOfCategory").text($(this).text());
	    $("#editCategory").show();
	    $("#deleteCategory").show();
	});
	
	//delete category request 
	$(document).on('click', '#deleteCategory',function(){
		$.ajax(url("/category/") + idCategory,{
			type: 'DELETE',
			success:function(){
				$('.categoryLists').remove();
				$('.badge').remove();
				$.get(url("/users/") + getUserID(), function(data){
					for(var j = 0; j < data.categories.length; j+=1){
						var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
						'</span><span class="badge">'+ data.categories[j].numberOfTasks +'</span></li>';
						var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
						$('#categories').append(category);
						$('.sel1').append(sameCategory);
						if(j === data.categories.length - 1){
							$('#' + data.categories[j].categoryId).trigger('click');
						}
					}
				});
			}
		});
	});
	//add category button
	$("#addCategoryButton").click(function(){
        $("#addCategory").show().focus();
        $("#addCategoryButton").css("display", "none");
    }).tooltip();
	
	//add category request
	$('#addCategory').keypress(function (e) {
	        var key = e.which;
	        var tmp = $("#addCategory");
	        if(key == 13){
	        	if(tmp.val() === ""){
	        		tmp.css("display", "none");
		            $("#addCategoryButton").css("display", "inline");   
	        		return;
	        	}
	        	var addCategory = {
	        			categoryName: tmp.val()
	        	}
	        	$.ajax(url("/category/") + getUserID(),{
	        		type: 'POST',
	                contentType: "application/json; charset=utf-8",
	                data:JSON.stringify(addCategory),
	                success:function(data){
	                	//console.log(data);
	                	var category = '<li class="ui-state-default"><span id="' + data.categoryId + '" class="categoryLists">' +
	                	data.categoryName + '<span class="badge">'+ data.numberOfTasks +'</span></span></li>';
	                	var sameCategory = '<option>'+ data.categoryName + '</option>';
	        			$('#categories').append(category);
	        			$('.sel1').append(sameCategory);
	                },
	                error:function(){
	                	alert("This name is taken!");
	                }
	        	});    
	        	tmp.css("display", "none").val("");
	            $("#addCategoryButton").css("display", "inline");   
	        }
	    });
	
	   
		//edit category request;
	    $('#editCategory').tooltip();
	    
	    $(document).on('click','.categoryLists', function(){
	    	idCategory = $(this).attr('id');
	    });
	    $(".editButton").click(function(){
	        $(".edit").hide();
	        $(".categoryList").hide();
	        $("#inputCategory").show();
	    });
	    $('#inputCategory').keypress(function (e) {
	        var key = e.which;
	        var tmp = $("#inputCategory");
	        if(key == 13){
	        	var objInput = {
	        			categoryName: tmp.val()
	        	}
	        	 $.ajax({
	     	        type: 'PUT', 
	     	        contentType: 'application/json; charset=utf-8', 
	     	        url: url('/category/' + idCategory), 
	     	        data: JSON.stringify(objInput),
	     	        success: function(data){
	     	        	$('.categoryLists').remove();
	    				$('.badge').remove();
	    				$.get(url("/users/") + getUserID(), function(data){
	    					//	console.log(data);
	    						for(var j = 0; j < data.categories.length; j+=1){
	    							var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
	    							'</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
	    							var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
	    							$('#categories').append(category);
	    							$('.sel1').append(sameCategory);
	    						}
	    					});
	     	        }
	     	    });
	        }
	    });
	   
	    $( "#categories" ).sortable({
	        revert: true
	    });
	    $("ul, li").disableSelection();
	    

	    
	    
	    
	    
	
	
	
	
    $("#sort").click(function(){
        $("#sorting").slideToggle("slow");
    });
    $("#addButton").click(function(){
    	$('#addModal').on('shown.bs.modal', function () {
  		  $('.description').focus();
    	});
        $("#addModal").modal();
    });
    $(".set1").mouseenter(function(){
        $(".set1").css("background-color", "#F5F5F5");
    }).mouseleave(function(){
        $(".set1").css("background-color", "white");
    });
    $("#accountSettings").click(function(){
        $("#settingsModal").modal();
    });
    
    $("#addTask").click(function(){
        $("#basicForm").slideDown();
        $("#basicAddTask").slideDown();
        $("#basicButton").show();
    });
    $(".addSubTask").click(function(){
        var subTaskForm = '<textarea class="subTaskText" name="subTask" cols="60" rows="1" placeholder="Subtask..."></textarea>';
        $("#subTask").append(subTaskForm);
    }).tooltip();
    $("#showCompleted").tooltip();
    $("#basicCheckboxLabel").tooltip();
    $("#goTop").tooltip();
    $(".sortBy").click(function(){
        $("#basicForm").hide();
        $("#basicButton").hide();
        $("#buttonsForCategories").hide();
        $(".subTaskText").hide();
        $('#inputCategory').hide();
    });
    $("#allTasks").click(function(){
        $("#basicForm").hide();
        $("#basicButton").hide();
        $("#buttonsForCategories").hide();
        $(".subTaskText").hide();
        $('#inputCategory').hide();
    });
    
    $("#basicAddTask").click(function(){
        $("#basicForm").hide();
        $("#basicButton").hide();
        $("#subTask").hide();
        $('#inputCategory').hide();
    });
    
    
    
	 $(".sortBy").click(function(){
	        var name = $("#" + this.id).text();
	        $("#nameOfCategory").text(name);
	        $('#editCategory').hide();
	        $('#deleteCategory').hide();
	    });
	    $("#allTasks").click(function(){
	        var name = $("#" + this.id).text();
	        $("#nameOfCategory").text(name);
	        $('#editCategory').hide();
	        $('#deleteCategory').hide();
	    });
	    $('#editCategory').click(function(){
	        $('#editCategory').hide();
	        $('#deleteCategory').hide();
	        $('#inputCategory').show().focus();
	        $('#inputCategory').keypress(function (e) {
	            var key = e.which;
	            if(key == 13){
	                var editCat = $('#inputCategory').val();
	                $('#nameOfCategory').text(editCat);
	                $('#inputCategory').hide().val("");
	                $('#editCategory').show();
	                $('#deleteCategory').show();
	                $('#nameOfCategory').show();
	            }
	        });
	    });
	   
	    
	    
    $( "#datepicker2" ).datepicker();
    $( "#datepicker1" ).datepicker();
    $('#timepicker1').timepicker();
    $('#timepicker2').timepicker();
    
    
    $(document).on('click','.categoryLists', function(){
		idCategory = $(this).attr('id');
	});
    $("#basicAddTask").click(function() {
    	var dateDue = dateOut($('#datepicker1').val(), $('#timepicker1').val());
        var tmpData = {
    		taskDescription : $('#basicDescription').val(),
    		dueDate : dateDue,
    		notificationDate : dateDue
        }
        if ($("#notification-box").is(':checked')) {
        	tmpData.notificationDate = dateNotification($('#datepicker1').val(), $('#timepicker1').val());
        }
	     $.ajax({
	      url : url("/task/" + idCategory),
	      type : 'POST',
	      contentType: "application/json; charset=utf-8",
	      data: JSON.stringify(tmpData),
	      success: function(data) {
	    	  console.log(data);
	    	  var newRow;
	    	  newRow = '<tr class="task-table-row"><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td>' + 
	    	  '<td><span class="descriptionTask">' + data.taskDescription + '</span></td>'+
	    	  '<td class="dateTable"><span class="date">'+ dateIn(data.dueDate) + '</span></td>'+
	    	  '<td class="timeTable"><span class="time">' + timeIn(data.dueDate) + '</span></td>'+
	    	  '<td class="menuArrow"><div class="dropdown">' + 
	    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
              '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
              '<ul class="dropdown-menu menuList"><li class="showBasicTask">Show</li>' +
              '<li class="editBasicTask">Edit</li><li class="deleteBasicTask">Delete</li>' + 
              '</ul></div></td></tr>';
		       $("#task-table").append(newRow);
		       $('.categoryLists').remove();
		       $('.badge').remove();	
		       $.get(url("/users/") + getUserID(), function(data){
					//	console.log(data);
				for(var j = 0; j < data.categories.length - 1; j+=1){
					if(data.categories[j].categoryName === data.categories[j+1].categoryName){
						continue;
					}
					var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
				'</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
				var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
				$('#categories').append(category);
				$('.sel1').append(sameCategory);
				}
		       });
	      }
	   }); 
    });
	     
	     $('#editNameButton').click(function(){
	     	$('#nameInput').show().focus();
	     	$('#saveNameButton').show();
	     	$('#editNameButton').hide();
	     	$('#currentName').hide();
	     });
	     $('#saveNameButton').click(function(){
	     	var obj = {
	         	userName: $('#nameInput').val(),
	     	}
	     	$('#nameInput').hide().val("");
	     	$('#saveNameButton').hide();
	     	$('#editNameButton').show();
	     	$('#currentName').show();
	     	$.ajax(url('/users/' + getUserID()),{
	  	        type: 'PUT', 
	  	        contentType: 'application/json; charset=utf-8', 
	  	        data: JSON.stringify(obj),
	  	        success: function(data){
	  	        	$("#currentName").text(data.userName); 
	  	        },
	     		error: function(){
	     			alert("Bad input!");
	     		}
	     	});
	     });
	     $('#editEmailButton').click(function(){
	     	$('#emailInput').show().focus();
	     	$('#saveEmailButton').show();
	     	$('#editEmailButton').hide();
	     	$('#currentEmail').hide();
	     });
	     $('#saveEmailButton').click(function(){
	     	
	     
	     	$('#emailInput').hide().val("");
	     	$('#saveEmailButton').hide();
	     	$('#editEmailButton').show();
	     	$('#currentEmail').show();
	     	$.ajax({
	  	        type: 'PUT', 
	  	        contentType: 'application/json; charset=utf-8', 
	  	        url: url('/users/' + getUserID()), 
	  	        data: JSON.stringify(obj),
	  	        success: function(data){
	  	        	console.log(data);
	  	        },
	     		error: function(){
	     			alert("Bad input!");
	     		}
	     	});
	     });
	     $('#editPassButton').click(function(){
	     	$('.hiddenDiv').show();
	     	$('#oldPassInput').focus();
	     	$('#savePassButton').show();
	     	$('#editPassButton').hide();
	     	$('#currentPass').hide();
	     	$('#passLabel').hide();
	     });
	     $('#savePassButton').click(function(){
	     	$('.hiddenDiv').hide();
	     	$('#newPassInput').val('');
	     	$('#oldPassInput').val('');
	     	$('#savePassButton').hide();
	     	$('#editPassButton').show();
	     	$('#currentPass').show();
	     	$('#passLabel').show();
	     });
	     
	     
	     
	     $('.menu').click(function(){
	         $('.menuList').slideToggle();
	     });
	     $('.deleteBasicTask').click(function(){
	        // delete zaqvka
	     });
	     $('.editBasicTask').click(function(){


	     });
	     
	     
	     
	     $(document).on('click','.categoryLists', function(){
	 		idCategory = $(this).attr('id');
	 		
	 		$.get(url('/category/' + idCategory),function(data){
	 			$(".task-table-row").remove();
	 			for(var i = 0; i < data.tasks.length; i+=1){
	 				var newRow;
	 		    	  newRow = '<tr class="task-table-row"><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td>' + 
	 		    	  '<td><span class="descriptionTask">' + data.tasks[i].taskDescription + '</span></td>'+
	 		    	  '<td class="dateTable"><span class="date">'+ dateIn(data.tasks[i].dueDate) + '</span></td>'+
	 		    	  '<td class="timeTable"><span class="time">' + timeIn(data.tasks[i].dueDate) + '</span></td>'+
	 		    	  '<td class="menuArrow"><div class="dropdown">' + 
	 		    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
	 	              '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
	 	              '<ul class="dropdown-menu menuList"><li class="showBasicTask">Show</li>' +
	 	              '<li class="editBasicTask">Edit</li><li class="deleteBasicTask">Delete</li>' + 
	 	              '</ul></div></td></tr>';
	 			       $("#task-table").append(newRow);
	 			}
	 		})
	 	});
});