$(document).ready(function() {
    $("#forgot-password-enter-email").css("display","none");
    $("#log-in").click(function () {
    	$('#login-modal').on('shown.bs.modal', function () {
    		  $('#username').focus();
    	});
    	$("#login-modal").modal();
 
    });
    $("#sign-up").click(function(){
    	$('#sign-up-modal').on('shown.bs.modal', function () {
  		  	$('#sign-up-username').focus();
    	});
        $("#sign-up-modal").modal();
    });
    $("#not-member").click(function(){
        $("#login-modal").css("display", "none");
        $("#sign-up-modal").modal();
    });
    $("#forgot-password").click(function(){
    	$("#login-modal").css("display", "none");
        $("#forgot-password-enter-email").modal();
    });
    $("#registration-button-butt").click(function(){
        var reg = {
            userName: $("#sign-up-username").val(),
            password: $("#sign-up-pass").val(),
            email: $("#sign-up-email").val(),
            firstName: $("#sign-up-name").val(),
            lastName: $("#signup-lastname").val()            
        };
        if (validate()) {
        	$.ajax("http://localhost:8080/DailyOgranizer/webapi/register", {
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                data:JSON.stringify(reg),
                success:function(data){
                	var id = data.userId;
                    sessionStorage.setItem('userId',id);
                    location.href="main-page.html";
                },
                error: function(){
                    alert("error");
                }
            });	
        }        
    });
    $("#button-login").click(function(){
        var log = {
            userName: $("#username").val(),
            password: $("#pass").val()           
        };
    	$.ajax("http://localhost:8080/DailyOgranizer/webapi/login", {
            method: 'POST',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(log),
            success: function(data){
            	var id = data.userId;
            	sessionStorage.setItem('userId', id);
            	console.log("sajdas");
            	location.href="main-page.html";
            },
            error: function(){
                alert("error");
            }
        });	
    });
    $("#button-forgotpass").click(function() {
    	$.ajax("http://localhost:8080/DailyOrganizer/forgotpassword", {
    		method: 'POST',
    	    contentType: "application/json; charset=utf-8",
    	    data: JSON.stringify({email: $("#sign-up-username").val()}),
    	    success: function(){
              location.href="home-page.html";
              $("#message-sent-email").css("display", "block");
              window.setTimeOut('document.getElementById("message-sent-email")', 30000);
          },
          error: function(){
              alert("error");
          }
    	});
    });
    $('#confirm-pass').keyup(validPassword);
});
function validPassword(){ 
    var confPass = $('#confirm-pass').val();
    var signPass = $('#sign-up-pass').val();
    if(confPass == signPass){
    	$('#confirm-pass').css('border-color', 'blue');
    }else{
    	$('#confirm-pass').css('border-color', 'red');
    }
}
   