$('#addTask').click(function() {
	var resultDate = "";
	var date = document.getElementById("#datepicker1");
	for (var i = 0; i < date.length; i++) {
		if (date.charCodeAt(i) == 47) {
			resultDate = resultDate + "-";
		} else {
			resultDate = resultDate + date.charAt(i);
		}
	}
	function toSeconds(time) {
		var parts = time.split(":");
		parts[2] = "00";
		return parts[0] * 3600 + parts[1] * 60 + parts[2] * 1;
	}
	var time = Math.abs(toSeconds(document.getElementById("#basic_example_2")));
	var difference = time - 3600;
	var resultTime = [Math.floor(difference / 3600),
			Math.floor((difference % 3600) / 60), difference % 60];
	resultTime = resultTime.map(function(v) {
		return v < 10 ? '0' + v : v;
	}).join(':');
	time = [Math.floor(time / 3600),
			Math.floor((time % 3600) / 60), time % 60];
	time = time.map(function(v) {
		return v < 10 ? '0' + v : v;
	}).join(':');
	var taskDateAndTime = resultDate + "T" + time;
	var taskDefaultNotification = resultDate + "T" + resultTime;
	var tmpData = {
		taskName : null,
		description : 'document.getElementById("basicDescription").value',
		dueDate : taskDateAndTime,
		notificationDate : false,
		listOfSubTasks : null
	}
	if (document.getElementById("#notification-box") == true) {
		tmpData.notificationDate = taskDefaultNotification;
	}
	$.ajax({
		url : "",
		type : "post",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(tmpData),
		success: function() {
		      //var subTaskForm = '<textarea class="subTaskText" name="subTask" cols="60" rows="1" placeholder="Subtask..."></textarea>';
			var taskList = [1,2,3,4];
			var newRow;
			for (var i = 0; i < taskList.length; i++) {
				newRow = '<tr><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td><td><span class="descriptionTask">' + DESCRIPTION + '</td></tr>'
			}
			$("body").append(newRow);
		}
	})
})