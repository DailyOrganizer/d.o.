package service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import database.HibernateUtil;
import model.SubTask;
import model.Task;

public class SubTaskService {

	private final SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
	
		public synchronized SubTask getSubTask(int id)
		{
			SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			SubTask subTask = (SubTask) session.get(SubTask.class, id);
			if(subTask != null)
			{
				session.getTransaction().commit();
				session.close();
				return subTask;
			}
			else
			{
				session.getTransaction().commit();
				session.close();
				return null;
			}
		}
		
		public synchronized SubTask addAndReturnSubTask(int taskId,SubTask subTask)
		{
			SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			Task task = (Task) session.get(Task.class, taskId);
			System.out.println(task.getTaskDescription());
			if(task != null)
			{
				System.out.println("ass");
				if(subTask != null)
				{
					System.out.println(subTask.getSubTaskName());
				subTask.setTask(task);
				//task.subTasks().add(subTask);
				session.save(subTask);
				session.getTransaction().commit();
				session.close();
				return subTask;
				}
				else {
					session.getTransaction().commit();
					session.close();
					return null;
				}
			}
			else
			{
				session.getTransaction().commit();
				session.close();
				return null;
			}
			
		}
		
		public synchronized boolean removeSubTask(int subTaskId)
		{
			SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
			Session session = sessionFactory.openSession();
			session.beginTransaction();
			SubTask subTask  =(SubTask) session.get(SubTask.class, subTaskId);
			if(subTask != null)
			{
				session.delete(subTask);
				session.getTransaction().commit();
				session.close();
				return true;
			}
			else{
				session.getTransaction().commit();
				session.close();
				return true;
			}
				
		}
		
		public boolean areAllSubTasksChecked(Task task)
		{
			Session session = sessionFactory.openSession();
			Transaction transaction = null;
			try
			{
				transaction = session.beginTransaction();
				for(SubTask subTask : task.getSubTasks())
				{
					if(!subTask.isSubTaskChecked())return false;
				}
			}catch (Exception e) {
			try {
				transaction.rollback();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
				} finally {
					session.close();
					
				}
			return true;
			
			
		}
		
		public synchronized SubTask updateSubTask(int subTaskId, SubTask subTask){
			
			Session session = sessionFactory.openSession();
			Transaction transaction = null;
			try
			{
				transaction = session.beginTransaction();
				SubTask subTaskFromDb = (SubTask) session.get(SubTask.class, subTaskId);
				if (subTaskFromDb != null) 
				{	
					subTaskFromDb.setSubTaskChecked(subTask.isSubTaskChecked());
					subTaskFromDb.setSubTaskName(subTask.getSubTaskName());
					Task task = subTaskFromDb.task();
					if(areAllSubTasksChecked(task))
					{
						task.setTaskChecked(true);
					}
					
					session.update(subTaskFromDb);
					session.getTransaction().commit();
					return subTaskFromDb;
				}
			}catch (Exception e) {
			try {
				transaction.rollback();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
				} finally {
					session.close();
				}
			return null;
			
		}	
}
