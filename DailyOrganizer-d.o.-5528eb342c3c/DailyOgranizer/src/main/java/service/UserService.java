package service;

import java.util.ArrayList;
import java.util.List;

import model.Category;
import model.Task;
import model.User;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dailyogranizer.DailyOgranizer.exceptions.NoSuchUserException;

import database.HibernateUtil;

public class UserService {

	private final SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();
	
	public User getUser(int id) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction  = session.beginTransaction();
			User user = (User) session.get(User.class, id);
			session.getTransaction().commit();			
			return user;
		}catch (Exception e) {
	         if (transaction!=null) 
	        	 	transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
		

	}
	public synchronized User updateUser(int userId,User user) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			User userFromDb = (User) session.get(User.class, userId);
			if (userFromDb != null) 
			{	
				userFromDb.setAllFields(user.getUserName() == null ?userFromDb.getUserName():user.getUserName(),
						user.getPassword() == null ?userFromDb.getPassword():user.getPassword(), 
								user.getEmail() == null ?userFromDb.getEmail():user.getEmail(),
										user.getFirstName() == null ?userFromDb.getFirstName():user.getFirstName(),
												user.getLastName() == null ?userFromDb.getLastName():user.getLastName());
				session.update(userFromDb);
				session.getTransaction().commit();
				return userFromDb;
			}
		}catch (Exception e) {
	         if (transaction!=null) 
	        	 transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
	}	
	

	public synchronized boolean isUserValid(User user) {
		
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			if (user.getUserName() == null || user.getEmail() == null || user.getPassword() == null
					|| this.getUserByEmail(user) != null || this.getUserByUserName(user) != null)
			{
				session.getTransaction().commit();
				return false;
			}
			else
			{
				session.getTransaction().commit();
				return true;
			}
		}catch (Exception e) {
	         if (transaction!=null) 
	        	 	transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return false;
	}

	public User getUserByEmail(User user) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Query query = session.createQuery("from User where email = :email ");
			query.setCacheable(true);
			query.setParameter("email", user.getEmail());
			@SuppressWarnings("unchecked")
			ArrayList<User> users = (ArrayList<User>) query.list();
			if (users.isEmpty()) {
				session.getTransaction().commit();
				return null;
			} else
				session.getTransaction().commit();
				return users.get(0);
		}catch (HibernateException e) {
	         if (transaction!=null) 
	        	 	transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
		
	}

	public User getUserByUserName(User user) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Query query = session.getNamedQuery("Users_byName");
			query.setCacheable(true);
			query.setParameter(0, user.getUserName());
			@SuppressWarnings("unchecked")
			ArrayList<User> users = (ArrayList<User>) query.list();
			if (users.isEmpty()) {
			session.getTransaction().commit();

			return null;
		} else
			session.getTransaction().commit();
			return users.get(0);
			
		}catch (Exception e) {
	         if (transaction!=null) 
	        	 	transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
		
	}

	public synchronized User addUserAndReturnIt(User user) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			if (user != null) {
			session.save(user);
			session.getTransaction().commit();
			return user;
		} else {
			session.getTransaction().commit();
			return null;
		}
		}catch (HibernateException e) {
	         if (transaction!=null) 
	        	 	transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
	}

	public synchronized void addDefaultCategories(User user) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Category events = new Category();
			events.setCategoryName("Events");
			events.setUser(user);
			user.getCategories().add(events);
			session.save(events);
	
			Category meetings = new Category();
			meetings.setCategoryName("Meetings");
			meetings.setUser(user);
			user.getCategories().add(meetings);
			session.save(meetings);
	
			Category payments = new Category();
			payments.setCategoryName("Payments");
			payments.setUser(user);
			user.getCategories().add(payments);
			session.save(payments);
	
			Category work = new Category();
			work.setCategoryName("Work");
			work.setUser(user);
			user.getCategories().add(work);
			session.save(work);
	
			Category sport = new Category();
			sport.setCategoryName("Sport");
			sport.setUser(user);
			user.getCategories().add(sport);
			session.save(sport);
			session.getTransaction().commit();
			
			}catch (Exception e) {
		         if (transaction!=null) 
		        	 	transaction.rollback();
		         e.printStackTrace(); 
		      }finally 
				{
		         session.close(); 
		      }

	}

	public User validateUser(User user) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			User newUser = this.getUserByUserName(user);
			if (newUser == null) {
				session.getTransaction().commit();
				throw new NoSuchUserException("There is no such user or the password is wrong");
			} else {
	
				if (user.getPassword().equals(newUser.getPassword())) {
					session.getTransaction().commit();
					return newUser;
				} else {
					session.getTransaction().commit();
					throw new NoSuchUserException("There is no such user or the password is wrong");
				}
			}
			}catch (HibernateException e) {
		         if (transaction!=null) 
		        	 	transaction.rollback();
		         e.printStackTrace(); 
		      }finally 
				{
		         session.close(); 
		      }
		return null;

		}

	public synchronized boolean deleteUser(int userId) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			User user = (User) session.get(User.class, userId);
			if (session.get(User.class, userId) != null) 
			{
				session.delete(user);
				session.getTransaction().commit();
				return true;
			}
			}catch (Exception e) {
		         if (transaction!=null) 
		        	 	transaction.rollback();
		         e.printStackTrace(); 
		      }finally 
				{
		         session.close(); 
		      }
		return false;
	
	}

	public String getPasswordForUser(String email) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			Query query = session.createQuery("from User where email = :email");
			query.setCacheable(true);
			query.setParameter("email", email);
			@SuppressWarnings("unchecked")
			ArrayList<User> users = (ArrayList<User>) query.list();
			if (users.isEmpty()) {
				session.getTransaction().commit();
				return null;
			} else
				session.getTransaction().commit();
				return users.get(0).getPassword();
		}catch (Exception e) {
	         if (transaction!=null) 
	        	 	transaction.rollback();
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
	}
	
	public ArrayList<Category> getCategories(int userId)
	{
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try{
			transaction = session.beginTransaction();
			Query query = session.createQuery("new ArrayList<Category> from Category where CategoryId = :userId");
			query.setCacheable(true);
			query.setParameter("category", userId);
			@SuppressWarnings("unchecked")
			ArrayList<Category> arrayList = (ArrayList<Category>) query.list();
			session.getTransaction().commit();
			return arrayList;
		}catch (Exception e) {
		         if (transaction!=null) 
		        	 	transaction.rollback();
		         e.printStackTrace(); 
		      }finally 
				{
		         session.close(); 
		      }
			return null;
			
	}


		public List<Task> returnAllTasks(int userId)
		{
			Session session = sessionFactory.openSession();
			Transaction transaction = null;
			try {
				ArrayList<Task> searched = new ArrayList<Task>();
				transaction = session.beginTransaction();
				Query query = session.createQuery("from Task");
				ArrayList<Task> tasks = (ArrayList<Task>) query.list();
				for (Task task : tasks) {
					if(task.category().user().getUserId() == userId)
					{
						searched.add(task);
					}
				}
				if (tasks != null) {
					session.getTransaction().commit();
					return searched;
				}
				session.getTransaction().commit();
				return null;
			} catch (Exception e) {
				try {
					transaction.rollback();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} finally {
				session.close();
			}
			return null;
		}
		}
