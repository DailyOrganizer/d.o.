package service;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.dailyogranizer.DailyOgranizer.exceptions.DataNotFoundException;

import model.Category;
import model.Task;
import model.User;
import database.HibernateUtil;

public class CategoryService {

	public CategoryService() {
	}
	
	private final SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();

	public  synchronized Category getCategory(int id) {	
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();	
			Category category = (Category) session.get(Category.class, id);
			if (category != null){
				session.getTransaction().commit();
				return category;
			}
			session.getTransaction().commit();
			return null;
		}catch (Exception e) {
			try
			{
				transaction.rollback();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
	        	
	         e.printStackTrace();
		      }finally 
				{
		         session.close(); 
		      }
			return null;
	}

	public synchronized Category addAndReturnCategory(int userId, Category category) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();	
			User user = (User) session.get(User.class, userId);
			if (user != null) {
				for (Category cat : user.getCategories()) 
				{
					if (cat.getCategoryName().equals(category.getCategoryName())) 
					{
						session.getTransaction().commit();
						return null;
					}
				}
					category.setUser(user);
					user.getCategories().add(category);
					session.save(category);
					session.getTransaction().commit();
					return category;
			} else 
			{
					session.getTransaction().commit();
					return null;
			}
			}catch (Exception e) {
		         try
				{
					transaction.rollback();
				}catch(Exception ex)
				{
					ex.printStackTrace();
				}
		        	
		         e.printStackTrace();
		      }finally 
				{
		         session.close(); 
		      }
			return null;

	}

	public synchronized boolean deleteCategory(int categoryId) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();	
			Category category = (Category) session.get(Category.class, categoryId);
			if (category == null) {
				session.getTransaction().commit();
				return false;
			} else {
				session.delete(category);
				session.getTransaction().commit();
				return true;
		}
		}catch (Exception e) {
			try
			{
				transaction.rollback();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
	        	
	         e.printStackTrace();
	      }finally 
			{
	         session.close(); 
	      }
		return false;
	}

	public synchronized Category updateCategory(int categoryId,Category category) {
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		System.out.println(categoryId);
		try
		{
			transaction = session.beginTransaction();
			Category categoryFromDb = (Category) session.get(Category.class, categoryId);
			if (categoryFromDb != null) 
			{	
				System.out.println("sddsd");
				categoryFromDb.setCategoryName(category.getCategoryName());
				session.update(categoryFromDb);
				session.getTransaction().commit();
				return categoryFromDb;
			}
		}catch (Exception e) {
			
			try
			{
				transaction.rollback();
			}catch(Exception ex)
			{	
				ex.printStackTrace();
			}
	        	
	         e.printStackTrace();
	      }finally 
			{
	         session.close(); 
	      }
		return null;
		
		

	}
	
	public ArrayList<Task> getTasks(int categoryId)
	{
		
		Session session = sessionFactory.openSession();
		Transaction transaction = null;
		try
		{
			transaction = session.beginTransaction();
			Query query = session.createQuery("new ArrayList<Task> from Task where taskId = :categoryId");
			query.setCacheable(true);
			query.setParameter("categoryId", categoryId);
			@SuppressWarnings("unchecked")
			ArrayList<Task> arrayList = (ArrayList<Task>) query.list();
			session.getTransaction().commit();
			return arrayList;
		}catch (HibernateException e) {
			try
			{
				transaction.rollback();
			}catch(Exception ex)
			{
				ex.printStackTrace();
			}
	        	
	         e.printStackTrace(); 
	      }finally 
			{
	         session.close(); 
	      }
		return null;
	}
}
