package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


@XmlRootElement
@Entity
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.TRANSACTIONAL)
@Table(name = "Categories", uniqueConstraints = @UniqueConstraint(columnNames = { "userid", "categoryname" }) )
public class Category implements Serializable {

	private static final long serialVersionUID = 7104687643898381246L;
	
	@ManyToOne
	@JoinColumn(name = "userid")
	@NotFound(action = NotFoundAction.IGNORE)
	private User user;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int categoryId;

	@NotNull
	@Column(nullable = false)
	private String categoryName;
	

	

	public void setNumberOfTasks(int numberOfTasks) {
		this.numberOfTasks = numberOfTasks;
	}

	@OneToMany(mappedBy = "category", fetch = FetchType.EAGER)
	@Cascade({CascadeType.DELETE})
	private List<Task> tasks = new ArrayList<Task>();
	
	@Transient
	private int numberOfTasks ;

	
	public Category() {
	}

	
	
	public User user()
	{
		return user;
	}
	
	public int getNumberOfTasks() {
		return numberOfTasks = tasks.size();
	}

	public List<Task> tasks()
	{
		return tasks;
	}
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Column(name = "categoryid")
	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name = "categoryname")
	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Object getListOfTask() {
		return null;
	}

	public void addTask(Task task) {
	}

	@Override
	public String toString() {
		return categoryName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + categoryId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Category other = (Category) obj;
		if (categoryId != other.categoryId)
			return false;
		return true;
	}
	public List<Task> getTasks() {
		return tasks;
	}
	
}
