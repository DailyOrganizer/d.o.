package model;

import java.util.ArrayList;

public class SearchTable {
	private ArrayList<Task> tasks;
	
	public SearchTable() {
		tasks = new ArrayList<Task>();
	}

	public ArrayList<Task> getTasks() {
		return tasks;
	}

	public void setTasks(ArrayList<Task> tasks) {
		this.tasks = tasks;
	}
	
	
}

