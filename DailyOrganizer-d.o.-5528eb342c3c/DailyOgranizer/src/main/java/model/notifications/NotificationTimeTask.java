package model.notifications;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.LocalDate;

import database.HibernateUtil;
import model.Task;

public class NotificationTimeTask extends TimerTask {
	private final SessionFactory sessionFactory = HibernateUtil.getInstance().getSessionFactory();

	@Override
	public synchronized void run() {
		Date date = LocalDate.now().toDate();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Query query = session
				.createQuery("from Task where notificationDate <= :date AND isNotificated =:isNotificated");
		query.setCacheable(true);
		query.setParameter("date", date);
		query.setParameter("isNotificated", false);
		ArrayList<Task> tasks = (ArrayList<Task>) query.list();
		for (Task task : tasks) {
			if (!task.isNotificated()) {
				String email = task.category().user().getEmail();
				System.out.println(email);
				new TaskNotification(email).sendEmail();
				task.setNotificated(true);
				session.update(task);

			}
		}
		session.getTransaction().commit();
		session.close();

	}

}
