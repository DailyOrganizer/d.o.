package model.notifications;

public class ForgottenPasswordNotification extends Notification{
	
	
	public ForgottenPasswordNotification(String receiver,String forgottenPassword) {
		super(receiver);
		emailSubject = "Forgotten Password";
		sender = "DailyOrganizerTeam@gmail.com";
		senderUserName = "DailyOrganizerTeam";
		senderPassword = "DailyOrganizer";
		emailMessage = "Your password is : "+forgottenPassword;     
		
	}

}
