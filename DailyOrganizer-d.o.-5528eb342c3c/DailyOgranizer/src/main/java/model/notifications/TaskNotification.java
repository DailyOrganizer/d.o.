package model.notifications;

public class TaskNotification extends Notification{
 private static final String MESSAGE = "You have a task that need you attention"; 
	public TaskNotification(String receiver) {
		super(receiver);
		emailSubject = "Task Pending";
		sender = "DailyOrganizerTeam@gmail.com";
		senderUserName = "DailyOrganizerTeam";
		senderPassword = "DailyOrganizer";
		emailMessage = MESSAGE;
	}

}
