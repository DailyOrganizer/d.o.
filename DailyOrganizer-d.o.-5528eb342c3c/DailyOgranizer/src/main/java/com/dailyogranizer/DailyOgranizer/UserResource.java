package com.dailyogranizer.DailyOgranizer;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import model.SearchTable;
import model.SubTask;
import model.Task;
import model.User;
import service.UserService;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

@Path("/users")
public class UserResource {
	
	UserService userService = new UserService();

	@Path("/{userId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("userId") int userId)
	{
		User user = userService.getUser(userId);
		if(user == null)
		{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}else
		{
			return  Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(user)
					.build();
		}
	}
	
	@Path("/{userId}")
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUser(@PathParam("userId") int userId, User user)
	{
			
		User updatedUser = userService.updateUser(userId,user);
		if(updatedUser !=null)			
		{
			return  Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(updatedUser)
					.build();
		}else
		{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("The update failed",403))
					.build();
		}
	}
	
	@Path("/{userId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public Response deleteUser(@PathParam("userId") int userId)
	{
		boolean isDeleted = userService.deleteUser(userId);
		if(!isDeleted)
		{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}else
		{
			return  Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}
	}
	
	@SuppressWarnings("unused")
	@Path("/alltasks/{userId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getallTasks(@PathParam("userId") int userId)
	{
		ArrayList<Task> tasks = (ArrayList<Task>) userService.returnAllTasks(userId);
		SearchTable searchTable = new SearchTable();
		for (Task task : tasks) {
			task.setSubTasks(new ArrayList<SubTask>());
		}
		searchTable.setTasks(tasks);
		if(searchTable == null)
		{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}else
		{
			return  Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(searchTable)
					.build();
		}
	}
	

	
}
