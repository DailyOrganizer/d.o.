package com.dailyogranizer.DailyOgranizer.exceptions;

public class MalformedJsonException extends RuntimeException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2297186534529132490L;

	public MalformedJsonException(String message)
	{
		super(message);
	}
}
