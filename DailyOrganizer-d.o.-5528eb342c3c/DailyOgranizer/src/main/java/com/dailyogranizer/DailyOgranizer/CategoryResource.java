package com.dailyogranizer.DailyOgranizer;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

import model.Category;
import model.Task;
import service.CategoryService;
import service.DataBaseStatus;
import service.UserService;

@Path("/category")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CategoryResource {

	CategoryService categoryService = new CategoryService();
	UserService userService = new UserService();

	
	 @Path("/{categoryId}")
	 @GET
	 public Response getTasks(@PathParam("categoryId") int categoryId)
	 {
		 	
		 	Category category = categoryService.getCategory(categoryId);
	
			if(category != null)
			{
				 return Response.status(Status.OK)
					 .header("Access-Control-Allow-Origin", "*")
					 .entity(category)
					 .build();
			}
			else
			{
				return Response.status(Status.UNAUTHORIZED)
						.header("Access-Control-Allow-Origin", "*")
						.entity(new ErrorMessage("There was an error in your request",403))
						.build();
				
			}
	 }
	
	 
	 @Path("/{categoryId}")
	 @PUT
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
	 public Response updateCategory(@PathParam("categoryId") int categoryId, Category
	 category)
	 {
		 Category updatedCategory = categoryService.updateCategory(categoryId,category);
		 System.out.println("sdsd");
		 if (category != null)
		 {
				 return Response.status(Status.OK)
					 .header("Access-Control-Allow-Origin", "*")
					 .entity(updatedCategory)
					 .build();
		 }
		 else
		 {
				return Response.status(Status.UNAUTHORIZED)
						.header("Access-Control-Allow-Origin", "*")
						.entity(new ErrorMessage("This update failed",403))
						.build();
		 }
	 }
	 
	
	@POST
	@Path("/{userId}")
	public Response addCategory(@PathParam("userId") int userId, Category category) {
				
		Category addedCategory = categoryService.addAndReturnCategory(userId, category);
		if (addedCategory != null) {
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(addedCategory)
					.build();
		} else{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}
	}
	
	@DELETE
	@Path("/{categoryId}")
	public Response deleteCategory(@PathParam("categoryId") int categoryId) {
			boolean isDeleted = categoryService.deleteCategory(categoryId);
			if(isDeleted)
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		 else {
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}
	}

}
