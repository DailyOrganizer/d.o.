package com.dailyogranizer.DailyOgranizer.exceptions;

public class UserAlreadyExistsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1015096107402999958L;

	public UserAlreadyExistsException(String message)
	{
		super(message);
	}
}
