package com.dailyogranizer.DailyOgranizer.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class MalformeJsonExceptionMapper implements ExceptionMapper<MalformedJsonException> {

	

	@Override
	public Response toResponse(MalformedJsonException exception) {
		ErrorMessage error = new ErrorMessage("The JSON is Malformed",403);
		return Response.status(Status.NOT_FOUND)
				.header("Access-Control-Allow-Origin", "*")
				.entity(error)
				.build();
	}

}
