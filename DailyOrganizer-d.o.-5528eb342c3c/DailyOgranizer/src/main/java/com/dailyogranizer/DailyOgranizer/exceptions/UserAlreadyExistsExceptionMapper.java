package com.dailyogranizer.DailyOgranizer.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class UserAlreadyExistsExceptionMapper implements ExceptionMapper<UserAlreadyExistsException>{

	@Override
	public Response toResponse(UserAlreadyExistsException exception) {
			
			ErrorMessage error = new ErrorMessage(exception.getMessage(),409);
			return Response.status(Status.CONFLICT)
					.header("Access-Control-Allow-Origin", "*")
					.entity(error)
					.build();
		
	}

	
}
