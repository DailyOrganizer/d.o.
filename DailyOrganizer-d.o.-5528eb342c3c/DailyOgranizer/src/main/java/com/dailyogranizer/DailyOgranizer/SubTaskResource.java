package com.dailyogranizer.DailyOgranizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

import model.SubTask;
import service.DataBaseStatus;
import service.SubTaskService;



@Path("/subtask")
public class SubTaskResource {

	SubTaskService subTaskService  = new SubTaskService();
	
	@Path("/{taskId}")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response addTask(@PathParam("taskId") int taskId,SubTask subTask)
	{
			SubTask sub = new SubTask();
			sub.setSubTaskName("subTask");
			SubTask  newSubTask = subTaskService.addAndReturnSubTask(taskId,sub);
			if(newSubTask != null)
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(newSubTask)
					.build();
			else

				return Response.status(Status.UNAUTHORIZED)
						.header("Access-Control-Allow-Origin", "*")
						.entity(new ErrorMessage("There was an error in your request",403))
						.build();
	}
	
	@Path("/{subTaskId}")
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response updateTask(@PathParam("subTaskId") int subTaskId,SubTask subTask)
	{
		SubTask updatedSubTask = subTaskService.updateSubTask(subTaskId,subTask);
		if(updatedSubTask != null)
		{
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.entity(updatedSubTask)
					.build();
		}
		else
		{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("This update failed",403))
					.build();
		}
	}
	
	@Path("/{subTaskId}")
	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response deleteTask(@PathParam("subTaskId") int subTaskId)
	{
		boolean isDeleted = subTaskService.removeSubTask(subTaskId);
		if(isDeleted)
		{
			return Response.status(Status.OK)
					.header("Access-Control-Allow-Origin", "*")
					.build();
			
		}else
		{
			return Response.status(Status.UNAUTHORIZED)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}
	}

}
