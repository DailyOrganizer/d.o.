package com.dailyogranizer.DailyOgranizer.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.hibernate.HibernateException;

public class HibernateExceptionMapper implements ExceptionMapper<HibernateException> {



	ErrorMessage error = new ErrorMessage("InternalSever Error",500);
	@Override
	public Response toResponse(HibernateException exception) {
		return Response.status(Status.NOT_FOUND)
				.header("Access-Control-Allow-Origin", "*")
				.entity(error)
				.build();
	}


	

}
