package com.dailyogranizer.DailyOgranizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

import model.User;
import service.UserService;

@Path("/login")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LoginResource {
	
	
	@POST
	public Response logIn(User user){
		
		if(user.getUserName() == null 
				|| user.getPassword() == null)
		{
			return Response.status(Status.CONFLICT).
					header("Access-Control-Allow-Origin :", "true")
					.entity(new ErrorMessage("The Json is malformed",403))
					.build();
		}
		
		UserService userService = new UserService();		
		User validUser = userService.validateUser(user);	
		
			return Response.status(Status.ACCEPTED)
					.entity(validUser)
					.header("Access-Control-Allow-Origin", "*")
					.entity(validUser)
					.build();
		}
				
	}
	
