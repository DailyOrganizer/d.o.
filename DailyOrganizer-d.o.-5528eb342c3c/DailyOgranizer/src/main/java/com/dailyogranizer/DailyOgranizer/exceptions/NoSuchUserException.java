package com.dailyogranizer.DailyOgranizer.exceptions;

public class NoSuchUserException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3999922977411448052L;
	
	public NoSuchUserException(String message)
	{
		super(message);
	}

}
