package com.dailyogranizer.DailyOgranizer.exceptions;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
@Provider
public class NoSuchUserExceptionMapper implements ExceptionMapper<NoSuchUserException>{

	@Override
	public Response toResponse(NoSuchUserException exception) {
		
		ErrorMessage error = new ErrorMessage("The JSON is Malformed",403);
		return Response.status(Status.NOT_FOUND)
				.header("Access-Control-Allow-Origin", "*")
				.entity(error)
				.build();
	}

}
