package com.dailyogranizer.DailyOgranizer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.dailyogranizer.DailyOgranizer.exceptions.ErrorMessage;

import service.UserService;
import model.User;
import model.notifications.ForgottenPasswordNotification;
import model.notifications.INotification;

@Path("/forgottenPassword")
public class EmailResource {
	
	UserService userService = new UserService();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response sendEmail(User user)
	{
		System.out.println("Krasi");
		String password = userService.getPasswordForUser(user.getEmail());
		if(password != null)
		{
			System.out.println("Ivan");
			INotification email = new ForgottenPasswordNotification(user.getEmail(), password);
			email.sendEmail();
			return Response.status(Status.OK)
				.header("Access-Control-Allow-Origin", "*")
				.build();
		}else
		{
			System.out.println("Not found");
			return Response.status(Status.FORBIDDEN)
					.header("Access-Control-Allow-Origin", "*")
					.entity(new ErrorMessage("There was an error in your request",403))
					.build();
		}
		
	}
}
