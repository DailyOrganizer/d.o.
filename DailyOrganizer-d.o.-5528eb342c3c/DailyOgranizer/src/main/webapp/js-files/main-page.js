// url function
function url(uri) {
	var path = "http://localhost:8080/DailyOgranizer/webapi";
	return path + uri;
}
// validate email with RegExp
function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
// get userId with sessionStorage
function getUserID(){
	var userID = sessionStorage.getItem('userId');
	return userID;
}
// server date format to our date format
function dateIn(fromServer){
    var date = "";
    var year = fromServer.slice(0,4);
    var month = fromServer.slice(5,7);
    var day = fromServer.slice(8,10);
    date = day + "/" + month + "/" + year;
    return date;
}
//server time format to our time format
function timeIn(fromServer){
    var clock = '';
    clock = fromServer.slice(11,16);
    return clock;
}
//console.log(dateIn("2014-06-07T13:23:03"));
//console.log(timeIn("2014-06-07T13:23:03"));
//our date format to server date format
function dateOut(dateFromMe, timeFromMe){
    var date = '';
    var day = dateFromMe.slice(3,5);
    var month = dateFromMe.slice(0,2);
    var year = dateFromMe.slice(6,10);
    date = year + "-" + month + "-" + day + "T" + timeFromMe + ":00";
    return date;
}
// console.log(dateOut("06/20/2015", "17:01"));
// date for notification
function dateNotification(dateFromMe, timeFromMe){
    var notifyTime = parseInt(timeFromMe.slice(0,2));
    var notifyMin = timeFromMe.slice(2,5);
    notifyTime -= 1;
    if(notifyTime < 10 && notifyTime >= 0){
    	notifyTime = "0" + notifyTime;
    }
    notifyTime = notifyTime + notifyMin;
    return dateOut(dateFromMe, notifyTime);
}
//console.log(dateNotification("06/20/2015", "17:01"));

/*
 * today in correct form
 * for today request
 */
function transformToday(){
	var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    if(dd < 10){
        dd = '0' + dd;
    }
    if(mm < 10){
        mm = '0' + mm;
    }
    today = yyyy + '-' + mm + '-' + dd + "T01:01:01";
    return today;
}

// get password
function getPass(){
	var pass = '';
	$.get(url("/users/") + getUserID(), function(data){
		pass = data.password;
	});
	return pass;
}

$(document).ready(function(){
	setTimeout(function() {
        $("#todaySort").trigger('click');
    },10);
	var idCategory = null;
	//get all data of user
	function getCategoryData(){
		// remove categories and badges 
		$('.categoryLists').remove();
		$('.badge').remove();
		//big request for all user information
		$.get(url("/users/") + getUserID(), function(data){
				for(var j = 0; j < data.categories.length; j+=1){
					var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + 
					data.categories[j].categoryName + '</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
					var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
					$('#categories').append(category);
					$('.sel1').append(sameCategory);
				}

				var greeting = "Hello, " + data.firstName + " " + data.lastName + "!";
				$('#hello').append(greeting);
				
				var currUserName = data.userName;
				$('#currentName').append(currUserName);
				var currPass = '';
				for(var i = 0; i < data.password.length; i += 1){
					currPass += '*';
				}
				$('#currentPass').append(currPass);
				var currEmail = data.email;
				$('#currentEmail').append(currEmail);
			});
	}
	getCategoryData();
	
	$(document).on('click','.categoryLists', function(){
		idCategory = $(this).attr('id');
		$("#buttonsForCategories").show();
	    $('#inputCategory').hide();
	    $("#nameOfCategory").text($(this).text()).css("text-transform", "capitalize");
	    $("#editCategory").show();
	    $("#deleteCategory").show();
	});
	
	//delete category request 
	$(document).on('click', '#deleteCategory',function(){
		$.ajax(url("/category/") + idCategory,{
			type: 'DELETE',
			success:function(){
				$('.categoryLists').remove();
				$('.badge').remove();
				$.get(url("/users/") + getUserID(), function(data){
					for(var j = 0; j < data.categories.length; j+=1){
						var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
						'</span><span class="badge">'+ data.categories[j].numberOfTasks +'</span></li>';
						var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
						$('#categories').append(category);
						$('.sel1').append(sameCategory);
						if(j === data.categories.length - 1){
							$('#' + data.categories[j].categoryId).trigger('click');
						}
					}
				});
			}
		});
	});
	
	//add category button
	$("#addCategoryButton").click(function(){
        $("#addCategory").show().focus();
        $("#addCategoryButton").css("display", "none");
    }).tooltip();
	
	//add category request
	$('#addCategory').keypress(function (e) {
	        var key = e.which;
	        var tmp = $("#addCategory");
	        if(key == 13){
	        	if(tmp.val() === ""){
	        		tmp.css("display", "none");
		            $("#addCategoryButton").css("display", "inline");   
	        		return;
	        	}
	        	var addCategory = {
	        			categoryName: tmp.val()
	        	}
	        	$.ajax(url("/category/") + getUserID(),{
	        		type: 'POST',
	                contentType: "application/json; charset=utf-8",
	                data:JSON.stringify(addCategory),
	                success:function(data){
	                	//console.log(data);
	                	var category = '<li class="ui-state-default"><span id="' + data.categoryId + '" class="categoryLists">' +
	                	data.categoryName + '<span class="badge">'+ data.numberOfTasks +'</span></span></li>';
	                	var sameCategory = '<option>'+ data.categoryName + '</option>';
	        			$('#categories').append(category);
	        			$('.sel1').append(sameCategory);
	                },
	                error:function(){
	                	alert("This name is taken!");
	                }
	        	});    
	        	tmp.css("display", "none").val("");
	            $("#addCategoryButton").css("display", "inline");   
	        }
	    });
		
		//edit category
		$('#editCategory').click(function(){
	        $('#editCategory').hide();
	        $('#deleteCategory').hide();
	        $('#inputCategory').show().focus();
	        $('#inputCategory').keypress(function (e) {
	            var key = e.which;
	            if(key == 13){
	                var editCat = $('#inputCategory').val();
	                $('#nameOfCategory').text(editCat);
	                $('#inputCategory').hide().val("");
	                $('#editCategory').show();
	                $('#deleteCategory').show();
	                $('#nameOfCategory').show();
	            }
	        });
	    });

		//edit category request;
	    $('#editCategory').tooltip();
	    
	    $(document).on('click','.categoryLists', function(){
	    	idCategory = $(this).attr('id');
	    });
	    $(".editButton").click(function(){
	        $(".edit").hide();
	        $(".categoryList").hide();
	        $("#inputCategory").show();
	    });
	    $('#inputCategory').keypress(function (e) {
	        var key = e.which;
	        var tmp = $("#inputCategory");
	        if(key == 13){
	        	var objInput = {
	        			categoryName: tmp.val()
	        	}
	        	 $.ajax({
	     	        type: 'PUT', 
	     	        contentType: 'application/json; charset=utf-8', 
	     	        url: url('/category/' + idCategory), 
	     	        data: JSON.stringify(objInput),
	     	        success: function(data){
	     	        	$('.categoryLists').remove();
	    				$('.badge').remove();
	    				$.get(url("/users/") + getUserID(), function(data){
    						for(var j = 0; j < data.categories.length; j+=1){
    							var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
    							'</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
    							var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
    							$('#categories').append(category);
    							$('.sel1').append(sameCategory);
    						}
    					});
	     	        }
	     	    });
	        }
	    });

	    // draggable categories
	    $( "#categories" ).sortable({
	        revert: true
	    });
	    $("ul, li").disableSelection();
	    

	//slide down and up sort ul
    $("#sort").click(function(){
        $("#sorting").slideToggle("slow");
    });
    
    // show add task modal
    $("#addButton").click(function(){
    	$('#addModal').on('shown.bs.modal', function () {
  		  $('.description').focus();
    	});
        $("#addModal").modal();
    });
    
    $(".set1").mouseenter(function(){
        $(".set1").css("background-color", "#F5F5F5");
    }).mouseleave(function(){
        $(".set1").css("background-color", "white");
    });
    $("#accountSettings").click(function(){
        $("#settingsModal").modal();
    });
    
    //basic form for add task
    $("#addTask").click(function(){
        $("#basicForm").slideDown();
        $("#basicAddTask").slideDown();
        $("#basicButton").show();
    });

    // all completed tasks ...
    $("#showCompleted").tooltip();
    $("#basicCheckboxLabel").tooltip();
    $("#goTop").tooltip();
    // hide things :)
    $(".sortBy").click(function(){
        $("#basicForm").hide();
        $("#basicButton").hide();
        $("#buttonsForCategories").hide();
        $(".subTaskText").hide();
        $('#inputCategory').hide();
    });
    $("#allTasks").click(function(){
        $("#basicForm").hide();
        $("#basicButton").hide();
        $("#buttonsForCategories").hide();
        $(".subTaskText").hide();
        $('#inputCategory').hide();
    });
    $("#basicAddTask").click(function(){
        $("#basicForm").hide();
        $("#basicButton").hide();
        $("#subTask").hide();
        $('#inputCategory').hide();
    });
	 $(".sortBy").click(function(){
	        var name = $("#" + this.id).text();
	        $("#nameOfCategory").text(name);
	        $('#editCategory').hide();
	        $('#deleteCategory').hide();
	    });
	    $("#allTasks").click(function(){
	        var name = $("#" + this.id).text();
	        $("#nameOfCategory").text(name);
	        $('#editCategory').hide();
	        $('#deleteCategory').hide();
	    });
	   
	 // date and time
    $("#datepicker2").datepicker();
    $("#datepicker1").datepicker();
    $('#timepicker1').timepicker();
    $('#timepicker2').timepicker();
    $("#datepicker3").datepicker();
    $('#timepicker3').timepicker();
    

    //add task request
    $(document).on('click','.categoryLists', function(){
		idCategory = $(this).attr('id');
	});
    $("#basicAddTask").click(function() {
    	var dateDue = dateOut($('#datepicker1').val(), $('#timepicker1').val());
        var tmpData = {
    		taskDescription : $('#basicDescription').val(),
    		dueDate : dateDue,
    		notificationDate : dateDue
        }
        if ($("#notification-box").is(':checked')) {
        	tmpData.notificationDate = dateNotification($('#datepicker1').val(), $('#timepicker1').val());
        }
	     $.ajax({
	      url : url("/task/" + idCategory),
	      type : 'POST',
	      contentType: "application/json; charset=utf-8",
	      data: JSON.stringify(tmpData),
	      success: function(data) {
	    	  var newRow;
	    	  newRow = '<tr class="task-table-row"><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td>' + 
	    	  '<td><span class="descriptionTask span-task">' + data.taskDescription + '</span></td>'+
	    	  '<td class="dateTable"><span class="date">'+ dateIn(data.dueDate) + '</span></td>'+
	    	  '<td class="timeTable"><span class="time">' + timeIn(data.dueDate) + '</span></td>'+
	    	  '<td class="menuArrow"><div class="dropdown">' + 
	    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
              '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
              '<ul class="dropdown-menu menuList"><li class="showBasicTask" id="' + data.taskId + "S" + '">Show</li>' +
              '<li class="editBasicTask" id="' + data.taskId + "E" + '">Edit</li><li class="deleteBasicTask" id=">Delete</li>' + 
              '</ul></div></td></tr>';
		       $('.categoryLists').remove();
		       $('.badge').remove();	
		       $.get(url("/users/") + getUserID(), function(data){
				for(var j = 0; j < data.categories.length; j+=1){
					if(idCategory !== data.categories[j].categoryId){
						var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
						'</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
						var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
						$('#categories').append(category);
						$('.sel1').append(sameCategory);
					}
				}
				$('#' + idCategory).trigger('click');
		       });
		       $('#' + idCategory).trigger('click');
	      },
	      error:function(){
	    	  alert("Bad request for add task!");
	      }
	   }); 
    });
	  //edit task
	    $(document).on('click','.categoryLists', function(){
	    	idCategory = $(this).attr('id');
	    });
    	$(document).on('click','.editBasicTask',function(){
    		idTask = $(this).attr('id');
    		idTask = idTask.substring(0, idTask.length - 1);	
    		$('#form-edit-task').show();
    		$.get(url('/category/' + idCategory),function(data){
	 			for(var i = 0; i < data.tasks.length; i+=1){
	 				if(idTask == data.tasks[i].taskId){
	 					console.log(data.tasks[i]);
	 					$('#editDescription').text(data.tasks[i].taskDescription);
	 					$('#datepicker3').val(dateIn(data.tasks[i].dueDate));
	 					$('#timepicker3').val(timeIn(data.tasks[i].dueDate));
//	 					if(data.tasks[i].taskChecked){
//	 						$('#editNotification').prop('checked', true);
//	 					}
	 				}
	 			}
	 		});
    		$('#editDescription').focus();
    	});
    	$('#saveTask').click(function(){
    		var obj = {
				taskDescription: $('#editDescription').val(),
			    dueDate: dateOut($('#datepicker3').val(), $('#timepicker3').val()),
			    notificationDate: dateOut($('#datepicker3').val(), $('#timepicker3').val())
    		}
    		if ($("#editNotification").is(':checked')) {
            	obj.notificationDate = dateNotification($('#datepicker3').val(), $('#timepicker3').val());
            }
    		$.ajax(url('/task/' + idTask),{
    			type: 'PUT',
    			contentType: 'application/json; charset=utf-8', 
	  	        data: JSON.stringify(obj),
	  	        success: function(data){
	  	        	$('#form-edit-task').hide();
	  	        	$('#' + idCategory).trigger('click');
	  	        },
	     		error: function(){
 					alert("Bad input!");
	     		}
    		});
    		
    		console.log($('.subtaskDescription').val());
    		var subTask = {
    			subTaskName: $('.subtaskDescription').val()
    		};
    		$.post(url('/subtask/') + idTask),{
    			contentType: "application/json; charset=utf-8",
        	    data: JSON.stringify(subTask),
        	    success: function(data){
        	    	console.log('data');
        	    	console.log(data);
        	    	var sub = '<p>' + data.subTaskName + '</p><span class="glyphicon glyphicon-pencil removeSubTask" id="' + subTaskId + "E" + '"></span>' +
        	    	'<span class="glyphicon glyphicon-remove deleteSubTask" id="' + subTaskId + 'D' + '></span>';
        	    	$('#showTask').append(sub);
        	   }
    		}
    	});
    	//edit subtask
    	$(document).on('click','.deleteSubTask', function(){
	   	   	idSubTask= $(this).attr('id');
	   	   	idSubTask = idSubTask.substring(0, idSubTask.length-1);
	   	   	$.ajax(url('/subtask/' + idSubTask),{
	   	   		type: 'DELETE',
	   	   		success: function(){
	   	   			//
	   	   		}
	   	   	});
    	});
    	
	  //delete basic task
	    $(document).on('click','.categoryLists', function(){
			idCategory = $(this).attr('id');
	    });
	    $(document).on('click','.deleteBasicTask', function(){
	   	 idTask= $(this).attr('id');
	   	 idTask = idTask.substring(0, idTask.length-1);
	   	 $.ajax(url('/task/' + idTask),{
	   		 type: 'DELETE',
	   		 success:function(){
				$('#' + idCategory).trigger('click');
				$('.categoryLists').remove();
				$('.badge').remove();
				$.get(url("/users/") + getUserID(), function(data){
					for(var j = 0; j < data.categories.length; j+=1){
						var category = '<li class="ui-state-default"><span id="' + data.categories[j].categoryId + '" class="categoryLists">' + data.categories[j].categoryName + 
						'</span><span class="badge">' + data.categories[j].numberOfTasks + '</span></li>';
						var sameCategory = '<option>'+ data.categories[j].categoryName + '</option>';
						$('#categories').append(category);
						$('.sel1').append(sameCategory);
					}
				});
	   		 }
	   	 });
	});
    
	   //show task
	    $(document).on('click','.showBasicTask',function(){
    		idTask = $(this).attr('id');
    		idTask = idTask.substring(0, idTask.length - 1);
    		$.get(url('/category/' + idCategory),function(data){
	 			for(var i = 0; i < data.tasks.length; i+=1){
	 				if(idTask == data.tasks[i].taskId){
	 					var taskDescr = data.tasks[i].taskDescription;
	 					var taskDate = dateIn(data.tasks[i].dueDate);
	 					var taskTime = timeIn(data.tasks[i].dueDate);
	 				}
	 			}
	 			var divTask = '<span class="glyphicon glyphicon-remove removeX"></span>'+
	 			'<p id="showDescription">' + taskDescr + 
		    	'</p><span id="showDate">' + taskDate + '</span>' +
		    		'<span id="showTime">' + taskTime + '</span>';
	 			$('#showTask').append(divTask);
	 			$('#showTask').show();
	 			location.hash = '#showTask';
	 		});
	    });
	    $(document).on('click','.glyphicon-remove',function(){
	    	$('#showTask').children().remove();
	    	$('#showTask').hide();
	    });
	   
	    //add subtask
	    
	    
	    
	    
	    //delete subtask
	    
	    
	    
	    
    	// edit and save username 
	     $('#editNameButton').click(function(){
	     	$('#nameInput').show().focus();
	     	$('#saveNameButton').show();
	     	$('#editNameButton').hide();
	     	$('#currentName').hide();
	     });
	     //request with enter
	     $('#nameInput').keypress(function (e) {
		        var key = e.which;
		        var tmp = $("#nameInput");
		        if(key == 13 && tmp.val() !== ""){
		        	var obj = {
	    	         userName: tmp.val()
		    	     };
			     	$('#nameInput').hide().val("");
			     	$('#saveNameButton').hide();
			     	$('#editNameButton').show();
			     	$('#currentName').show();
		        	$.ajax(url('/users/' + getUserID()),{
			  	        type: 'PUT', 
			  	        contentType: 'application/json; charset=utf-8', 
			  	        data: JSON.stringify(obj),
			  	        success: function(data){
			  	        	$("#currentName").text(data.userName); 
			  	        },
			     		error: function(){
	     					alert("Bad input!");
			     		}
			     	});
		        }
		    });
		 
	     //request with click on save button
	     $('#saveNameButton').click(function(){
	    	 var tmp = $("#nameInput");
	    	 if(tmp.val() !== ""){
	    		 var obj = {
		         	userName: $('#nameInput').val()
		     	}
		     	$('#nameInput').hide().val("");
		     	$('#saveNameButton').hide();
		     	$('#editNameButton').show();
		     	$('#currentName').show();
		     	$.ajax(url('/users/' + getUserID()),{
		  	        type: 'PUT', 
		  	        contentType: 'application/json; charset=utf-8', 
		  	        data: JSON.stringify(obj),
		  	        success: function(data){
		  	        	console.log(data);
		  	        	$("#currentName").text(data.userName); 
		  	        },
		     		error: function(){
		     			$('#emailInput').css("border-color","red");
		     		}
		     	});
	    	 }
	     });
	     // edit and save email
	     $('#editEmailButton').click(function(){
	     	$('#emailInput').show().focus();
	     	$('#saveEmailButton').show();
	     	$('#editEmailButton').hide();
	     	$('#currentEmail').hide();
	     });
	     //with enter
	     $('#emailInput').keypress(function (e) {
	        var key = e.which;
	        var tmp = $("#emailInput");
		     if(key == 13){
		        var obj = {
		        	email: tmp.val()
		    	};
	        	$.ajax(url('/users/' + getUserID()),{
		  	        type: 'PUT', 
		  	        contentType: 'application/json; charset=utf-8', 
		  	        data: JSON.stringify(obj),
		  	        success: function(data){
		  	        	if(validateEmail(data.email)){
		  	        		$('#emailInput').hide().val("");
		  	        		$('#currentEmail').text(data.email);
		  	        		$('#editEmailButton').show();
		  	        		$('#currentEmail').show();
		  	        		$('#saveEmailButton').hide();
		  	        	}else{
		  	        		$('#emailInput').css("border-color","red");
		  	        	} 
		  	        },
		     		error: function(){
		     			$('#emailInput').css("border-color","red");
		     			console.log("error");
		     		}
		     	});
	        }
	     });
	     //with click
	     $('#saveEmailButton').click(function(){
	    	 var obj = {
     			email: $('#emailInput').val()
	    	 };    	
	     	$.ajax(url('/users/' + getUserID()),{
	  	        type: 'PUT', 
	  	        contentType: 'application/json; charset=utf-8', 
	  	        data: JSON.stringify(obj),
	  	        success: function(data){
	  	        	if(validateEmail(data.email)){
	  	        		$('#emailInput').hide().val("");
	  	        		$('#currentEmail').text(data.email);
	  	        		$('#editEmailButton').show();
	  	        		$('#currentEmail').show();
	  	        		$('#saveEmailButton').hide();
	  	        	}else{
	  	        		$('#emailInput').css("border-color","red");
	  	        	}
	  	        },
	  	        error: function(){
	  	        	$('#emailInput').css("border-color","red");
	     		}
	     	});
	     });
	     
	     //edit and save password only with click
	     $('#editPassButton').click(function(){
	     	$('.hiddenDiv').show();
	     	$('#oldPassInput').focus();
	     	$('#savePassButton').show();
	     	$('#editPassButton').hide();
	     	$('#currentPass').hide();
	     	$('#passLabel').hide();
	     });
	     $('#oldPassInput').keyup(validPassword);
	     $('#savePassButton').click(function(){
	    	 var obj = {
	    		password: $('#newPassInput').value	 
	    	 };
	    	 $.ajax(url('/users/' + getUserID()),{
		  	        type: 'PUT', 
		  	        contentType: 'application/json; charset=utf-8', 
		  	        data: JSON.stringify(obj),
		  	        success: function(data){
		  	        	var pass = $('#newPassInput').val();
		  	        	var tmp = '';
		  	        	for(var i = 0; i < pass.length; i += 1){
		  	        		tmp += '*';
		  	        	}
		  	        	$('#currentPass').text(tmp);
		  	        	$('.hiddenDiv').hide();
		  		     	$('#newPassInput').val('');
		  		     	$('#oldPassInput').val('');
		  		     	$('#savePassButton').hide();
		  		     	$('#editPassButton').show();
		  		     	$('#currentPass').show();
		  		     	$('#passLabel').show();
		  	        },
		  	        error: function(){
		  	        	$('#oldPassInput').css("border-color","red");
		  	        }
		     	});
	     });
	     	     
	     
	     $('.menu').click(function(){
	         $('.menuList').slideToggle();
	     });
	     
	     $('.editBasicTask').click(function(){


	     });
	     
	     
	     // click on category and table with all tasks will show
	     $(document).on('click','.categoryLists', function(){
	 		idCategory = $(this).attr('id');
	 		$.get(url('/category/' + idCategory),function(data){
	 			$(".task-table-row").remove();
	 			if(data.tasks.length == 0){
	 				$('#noTasksDiv').show();
	 			}else{
	 				$('#noTasksDiv').hide();
	 			}
	 			for(var i = 0; i < data.tasks.length; i+=1){
	 				var newRow;
	 				newRow = '<tr class="task-table-row"><td id="checkbox"><input id="checkbox" type="checkbox"/></td>' + 
	 		    	  '<td><span class="descriptionTask">' + data.tasks[i].taskDescription + '</span></td>'+
	 		    	  '<td class="dateTable"><span class="date">'+ dateIn(data.tasks[i].dueDate) + '</span></td>'+
	 		    	  '<td class="timeTable"><span class="time">' + timeIn(data.tasks[i].dueDate) + '</span></td>'+
	 		    	  '<td class="menuArrow"><div class="dropdown">' + 
	 		    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
	 	              '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
	 	              '<ul class="dropdown-menu menuList"><li class="showBasicTask" id="'+ data.tasks[i].taskId + "S"+ '">Show</li>' +
	 	              '<li class="editBasicTask" id="'+ data.tasks[i].taskId + "E"+ '">Edit</li>'+
	 	              '<li class="deleteBasicTask" id="'+ data.tasks[i].taskId + "D"+ '">Delete</li>' + 
	 	              '</ul></div></td></tr>';
	 			       $("#task-table").append(newRow);
	 			}
	 		});
	 	});
	     
	     // click ot sorting and table is here...
	     $(document).on('click','.sortBy', function(){
	    	 var sortUrl = null;
	    	 idCategory = $(this).attr('id');
	    	 
		 	 switch(idCategory){
			 	 case 'todaySort': sortUrl = 'day'; break;
			 	 case 'weekSort': sortUrl = 'week'; break;
			 	 case 'monthSort': sortUrl = 'month'; break;
		 	 }
		 	 var today = new Date();
		 	 var obj = {
		 		date: transformToday()	 
		 	 };
		 	 $.ajax(url('/search/' + sortUrl),{
		 		type : 'POST',
			    contentType: "application/json; charset=utf-8",
			    data: JSON.stringify(obj),
			    success: function(data) {
			    	if(data.tasks.length == 0){
		 				$('#noTasksDiv').show();
		 			}else{
		 				$('#noTasksDiv').hide();
		 			}
			    	$(".task-table-row").remove();
		 			for(var i = 0; i < data.tasks.length; i+=1){
		 				var newRow;
		 				 newRow = '<tr class="task-table-row"><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td>' + 
		 		    	  '<td><span class="descriptionTask">' + data.tasks[i].taskDescription + '</span></td>'+
		 		    	  '<td class="dateTable"><span class="date">'+ dateIn(data.tasks[i].dueDate) + '</span></td>'+
		 		    	  '<td class="timeTable"><span class="time">' + timeIn(data.tasks[i].dueDate) + '</span></td>'+
		 		    	  '<td class="menuArrow"><div class="dropdown">' + 
		 		    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
		 	              '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
		 	              '<ul class="dropdown-menu menuList"><li class="showBasicTask" id="'+ data.tasks[i].taskId + "S"+ '">Show</li>' +
		 	              '<li class="editBasicTask" id="'+ data.tasks[i].taskId + "E"+ '">Edit</li>'+
		 	              '<li class="deleteBasicTask" id="'+ data.tasks[i].taskId + "D"+ '">Delete</li>' + 
		 	              '</ul></div></td></tr>';
		 			       $("#task-table").append(newRow);
		 			}
			    }
		 	 });
	     });		
	     
	     // click on all tasks if you want to see user's all tasks 
	     $('#allTasks').click(function(){
	    	$.get(url('/users/alltasks/' + getUserID()),function(data){
	    		$(".task-table-row").remove();
	 			for(var i = 0; i < data.tasks.length; i+=1){
	 				var newRow;
	 				 newRow = '<tr class="task-table-row"><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td>' + 
	 		    	  '<td><span class="descriptionTask">' + data.tasks[i].taskDescription + '</span></td>'+
	 		    	  '<td class="dateTable"><span class="date">'+ dateIn(data.tasks[i].dueDate) + '</span></td>'+
	 		    	  '<td class="timeTable"><span class="time">' + timeIn(data.tasks[i].dueDate) + '</span></td>'+
	 		    	  '<td class="menuArrow"><div class="dropdown">' + 
	 		    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
	 	              '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
	 	              '<ul class="dropdown-menu menuList"><li class="showBasicTask" id="'+ data.tasks[i].taskId + "S"+ '">Show</li>' +
	 	              '<li class="editBasicTask" id="'+ data.tasks[i].taskId + "E"+ '">Edit</li>'+
	 	              '<li class="deleteBasicTask" id="'+ data.tasks[i].taskId + "D"+ '">Delete</li>' + 
	 	              '</ul></div></td></tr>';
	 			       $("#task-table").append(newRow);
	 			}
	    	});
	     });
	     // fast add
	     $(document).on('click','.categoryLists', function(){
	 		idCategory = $(this).attr('id');
	 	});
	     $("#addTaskModal").click(function() {
	     	var dateDue = dateOut(transformToday(), $('#timepicker2').val());
	         var tmpData = {
	     		taskDescription : $('#description').val(),
	     		dueDate : dateDue,
	     		notificationDate : dateDue
	         }
	         if ($("#notification-box").is(':checked')) {
	         	tmpData.notificationDate = dateNotification(transformToday(), $('#timepicker1').val());
	         }
	 	     $.ajax({
	 	      url : url("/task/" + idCategory),
	 	      type : 'POST',
	 	      contentType: "application/json; charset=utf-8",
	 	      data: JSON.stringify(tmpData),
	 	      success: function(data) {
	  				var newRow;
	  				newRow = '<tr class="task-table-row"><td id="checkboxTable"><input id="checkbox" type="checkbox"/></td>' + 
	  	    	  '<td><span class="descriptionTask span-task">' + data.taskDescription + '</span></td>'+
	  	    	  '<td class="dateTable"><span class="date">'+ dateIn(data.dueDate) + '</span></td>'+
	  	    	  '<td class="timeTable"><span class="time">' + timeIn(data.dueDate) + '</span></td>'+
	  	    	  '<td class="menuArrow"><div class="dropdown">' + 
	  	    	  '<button class="dropdown-toggle menu" type="button" data-toggle="dropdown">'+
	                '<span class="menu glyphicon glyphicon-chevron-down"></span></button>' + 
	                '<ul class="dropdown-menu menuList"><li class="showBasicTask" id="' + data.taskId + "S" + '">Show</li>' +
	                '<li class="editBasicTask" id="' + data.taskId + "E" + '">Edit</li><li class="deleteBasicTask" id=">Delete</li>' + 
	                '</ul></div></td></tr>';
	  				$("#task-table").append(newRow);
	  				$('#' + idCategory).trigger('click');
	 	      },
	 	      error:function(){
	 	    	  alert("Bad request for add task!");
	 	      }
	 	  });
	   }); 		     
});
//check if current password is same as input password
function validPassword(){ 
	var oldPass = $('#oldPassInput').val();
    var pass = getPass();
    if(oldPass == pass){
    	$('#oldPassInput').css('border-color', 'blue');
    }else{
    	$('#oldPassInput').css('border-color', 'red');
    }
}

